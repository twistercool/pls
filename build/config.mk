VERSION = 0.1
PREFIX ?= /usr/local
INCS = -I ../include
LIBS = -lpierre
CFLAGS += -std=c17 ${INCS} -DVERSION=\"${VERSION}\" -Ofast
LDFLAGS += ${LIBS}
DEBUG_CFLAGS = ${CFLAGS} -O0 -g -ggdb -Wall -Wextra -Wno-unused-parameter
CC = gcc
STRIP ?= strip
