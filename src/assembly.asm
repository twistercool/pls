.section .data
dotStr:
   .asciz "."
slashStr:
   .asciz "/"
spaceStr:
   .asciz " "
colonNewline:
   .asciz ":\n"
newLine:
   .asciz "\n"
firstHalfStr:
   .asciz "pls: cannot access \'"
secHalfStr:
   .asciz "\': No such file or directory\n"
.include "../src/header.h"



.section .text
.global main

main:
   //this is copied from dissassembly
   push %rbp
   mov %rsp, %rbp
   sub $0x10, %rsp
   mov %edi, 0x4(%rbp)
   mov %rsi, -0x10(%rbp)
   mov $0, %eax
   //end of copied shit
   call mainImplement
   mov $0, %eax
   leave
   ret


mainImplement:
   cmpl $1, %edi
   je ifargc1
   cmpl $2, %edi
   je ifargc2


//for loop
   //init i = 0
   //i is r12
   mov $1, %r12

forLabel:
   push %rdi
   push %rsi

   //mov (%r13), %rdi
   mov (%rsi, %r12, 8), %rdi
   mov $1, %rsi

   call printDirEntries

   cmp $0, %eax
   jne leave

   pop %rsi
   pop %rdi

   //increments i
   add $1, %r12
   cmpl %edi, %r12d
   jge endfor
   jmp forLabel

endfor:

   movl $0, %edi
   call exit
   ret

leave:
   mov %eax, %edi
   call exit
   ret

ifargc1:
//pos. independent code for getting dotStr
   lea dotStr(%rip), %rdi
   mov $0, %rsi
   call printDirEntries

   mov $0, %rdi
   call exit
   ret

ifargc2:
//loading the address of argv[1]
   mov 8(%rsi), %rdi
   mov $0, %rsi
   call printDirEntries

   mov $0, %rdi
   call exit
   ret


printDirEntries:
   //dir_ptr is r14
   //dir_str is in rdi
   //dispDirLabel is in esi
   mov %rdi, %r15

   push %rsi

   call opendir

   pop %rsi

   mov %rax, %r14

   cmp $0, %r14
   je leaveAndCannotAccess

   //now print if dispDirLabel
   cmpl $0, %esi
   jne dispDirLabel
endIfDispDir:

   //while cur_ent = readdir(dir_ptr)
whileLoop:

   mov %r14, %rdi
   call readdir

   cmp $0, %rax
   je exitWhile



   //optimise below with one call for printf
   mov %rax, %rdi
   add $19, %rdi
   call printf

   lea spaceStr(%rip), %rdi
   call printf

   jmp whileLoop


exitWhile:

   mov %r14, %rdi
   call closedir
   lea newLine(%rip), %rdi
   call printf
   mov $0, %rax
   ret



dispDirLabel:
   mov %r15, %rdi
   call printf
   lea colonNewline(%rip), %rdi
   call printf
   jmp endIfDispDir


leaveAndCannotAccess:
   //make the string "pls: cannot access '%s': No such file or directory\n"
   lea firstHalfStr(%rip), %rdi
   call printf

   mov %r15, %rdi
   call printf
   lea secHalfStr(%rip), %rdi
   call printf
   mov $1, %rdi
   call exit
   ret
